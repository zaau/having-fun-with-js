
function sumNestedNumbers(arr) {
    if (arr.length < 0) return;

    var sum = 0;

    for (var i = 0; i < arr.length; i += 1) {
        var el = arr[i];
        if (Array.isArray(arr[i])) {
            sum = sum + arr[i][0] * sumNestedNumbers(arr[i]);

        } else {
            sum = sum + arr[i];
        }
    }

    return sum;


}
console.log(sumNestedNumbers([1, [2], 3]))