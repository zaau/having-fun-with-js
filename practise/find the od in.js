function findOdd(arr) {
    var count = 0;
    var maxKey;
    var a2 = {};
    for (var i = 0; i < arr.length; i += 1) {
        var numberStr = arr[i].toString();

        if (a2[numberStr] === undefined) {
            a2[numberStr] = 0;
        }

        a2[numberStr] = a2[numberStr] + 1;
    }

    for (var key in a2) {
        if (a2[key] % 2 !== 0) {

            return key;
        }
    }

}
var a = findOdd([1, 1, 1, 1, 1, 1, 10, 1, 1, 1, 1]);
console.log(a);