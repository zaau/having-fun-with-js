
var separator = ' ';
function garden(components) {
    var elms = components.split(separator);
    var back = elms.map(function (value) {
        if (value == "gravel" || value == "rock") return value;

        return "gravel";

    });
    return back.join(' ');
}
console.log(garden("garden rock snail"))