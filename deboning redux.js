
state = {
    todos: {
        'name': {
            value: '',
            isValid: false
        },
        'email': {
            value: '',
            isValid: false
        }
    },
    validation: {
        message: 'Everything is ok'
    }
};
const createStore = function (reducer) {
    console.log("reducer: ", reducer)
    let state;

    const getState = function () { return state; }

    const dispatch = function (data) {
        state = reducer(state, data);
    }

    dispatch({});

    return { dispatch: dispatch, getState: getState };
}

const todos = function (stateparam = state.todos, data = {}) {
    return Object.assign({}, stateparam, todo({
        value: '',
        isValid: false
    }, { value: "yra" }));
}
const todo = function (oldObject = {}, changedObject = {}) {
    return Object.assign({}, oldObject, changedObject);
}
const combineReducers = function (reducers) {
    return function (state = {}, data) {
        return Object.keys(reducers).reduce(
            function (nextstate, key) {
                nextstate[key] = reducers[key](
                    state[key],
                    data
                );
                return nextstate;
            },
            {}
        );
    };
};

const todoApp = combineReducers({
    todos: todos
});
let store = createStore(todoApp);
store.dispatch();

