
function log(arg) {
    document.writeln(arg)
}

function identity(x) {
    return x;
}

function add(a, b) {
    return a + b;
}
function sub(a, b) {
    return a - b;
}
function mul(a, b) {
    return a * b;
}

//var three= identityf(3);
//three(); //3
function identityf(a) {
    return function () {
        return a;
    }
}

addf(4)(5)//9
function addf(a) {
    return function (b) {
        return a + b;
    }

}

function liftf(fn) {
    return function (a) {
        return function (b) {
            return fn(a, b);
        }
    };
}

//var addf1 = liftf(add);
//log(addf1(3)(4))
//log(liftf(mul)(3)(4))
//this is example higher order functions which takes functions as parameters and returns functions as results

//####Curry function
//process of taking function with multiple arguments and creating multiple functions with single parameter is called curring
function curry(fn, a) {
    return function (b) {
        return fn(a, b)
    }
}
//Other way
function curry2(fn, a) {
    return liftf(fn)(a);
}
//var  add3 = curry2(add, 3);
//log(add3(4));
//var add3 = curry(add, 3);
//log(add3(4));
//log(curry(mul, 5)(6));

//curry in ES6
// Elipsis feature
function curry(func, ...first) {
    return function (...second) {
        return func(...first, ...second);
    }
}

//var inc= ___;
//inc(5)//6
//inc(inc(5))//7

//?????without new functions write inc function which does this:
//inc(5)//6
//inc(inc(5))//7

var inc = curry2(add, 1);
//log(inc(5));
//log(inc(inc(5)))
var inc2 = liftf(add)(1);
//log(inc2(5));
//log(inc2(inc2(5)));
var inc3 = addf(1);
//log(inc3(5));
//log(inc3(inc3(5)));

//????? Write function twice that takes a binary function and returns a unary function that passes its arguments to the binary function twice;
// add(11,11)
// var double = twice(add);
//double(11)//22
//var square = twice(mul);
//square(11)// 121

var twice = function (fn) {
    return function (a) {
        return fn(a, a);
    }
}
var double = twice(add);
//log(double(11));
var square = twice(mul);

//Write reverse, a function that reverses the arguments of binary function
//var bus = reverse(sub);
//bus(3,2);//-1

function reverse2(fn) {
    return function (a, b) {
        return fn(b, a);
    }
}
//var bus = reverse2(sub);
//log(bus(3,2));

//practical stuff
function reverse(func) {
    return function (...args) {
        return func(...args.reverse());
    }
}
var bus2 = reverse(sub);
//log(bus2(8,2,18));

//??? Write a function composeu that takes two unary functions and returns a unary function that call them both;
//composeu(double, square)(5)//100

function composeu(fn1, fn2) {
    return function (x) {
        return fn2(fn1(x));
    }
}
//log(composeu(double, square)(5));

//??? Write a function composeb that takes two binary functions and returns a function that calls them both;
//composeb(add, mul)(2,3,7);

function composeb(fn1, fn2) {
    return function (a, b, c) {
        return fn2(fn1(a, b), c);
    }
}
//log(composeb(add, mul)(2,3,7));


//??? Write a limit function that allows a binary function to be called a limited numbers of time
//var add_ltd = limit(add, 1);
//add_ltd(4,3);//7
//add_ltd(3,5)//undefined

function limit(fn, x) {
    //x stays in limit function lexical scope because of closure
    return function (a, b) {
        if (x >= 1) {
            x -= 1;
            return fn(a, b);
        }
        return undefined;

    }


}
//var add_ltd = limit(add, 2);
//log(add_ltd(4,3));
//log(add_ltd(3,5));

//??? Write a from function that produces a generator that will produce a series of values
//var index = from(0);
//index() //0
//index() //1
//index() //2

function from(x) {
    return function () {
        var next = x;
        x += 1
        return next;
    }
}
/*
var index= from(0);
log(index());
log(index());
*/
//??? Write a to function taht takes a generator and an end value, and return a generator that will produce numbers up to that limit
/*
var index = to(from(1),3);
index()//1
index()//2
index()//undefined
*/

function to(fn, x) {
    return function () {
        var y = fn();
        if (y >= x) {
            return undefined;
        } else {
            return y;
        }
        //better
        /*  
            var value = fn();
            if(value < end){
                return value;
            }
            return undefined;
        */


    }
}
/*
var index = to(from(1),3);
log(index());//1
log(index());//2
log(index());//undefined
log(index());
*/

//?? FromTo function
/*
    var index = fromTo(0,3);
    index()//0
    ...
    index()//2
    index()//undefined
*/

function fromTo(a, b) {

    return function () {
        if (a < b) {
            var current = a;
            a += 1;
            return current;
        }
        return undefined;
    }
}

function fromTo2(a, b) {
    return to(from(a), b)
}

/*var index1 = fromTo2(0,3);
log(index1());
log(index1());
log(index1());
log(index1());*/

//Write an element function that takes an array and a generator and returns a generator that will produce elements from array
/*
    var ele = element(['a','b','c','d'],fromTo(1,3));
    ele()//b
    ele()//c
    ele()/undefined
*/

function element(arr, fn) {
    return function () {
        //return arr[fn()];//it works but les handling it works out of the box
        var index = fn();
        if (index !== undefined) return arr[index];
    }
}
//Modified element function without passing generator
function element2(arr, fn) {
    if (fn === undefined) {
        fn = fromTo2(0, arr.length);
    }
    return function () {
        //return arr[fn()];//it works but les handling it works out of the box

        var index = fn();
        if (index !== undefined) return arr[index];
    }
}
/*
             var ele = element(['a','b','c','d'], fromTo(1,3));
                log(ele());
                log(ele());
                log(ele());
                log(ele());
                log(ele());
                log(ele());
            */
//??? Write a collect function that takes a generator and an array and produces a function that will collect the results in the array
/*
    var array = [],
        col = collect(fromTo(0,2), array);
        col()//0;
        col()//1
        col()//undefined
        array//[0,1]
*/
function collect(fn, arr) {

    return function () {
        var index = fn();
        if (index !== undefined) {
            arr.push(index);
        }
        return index;

    }
}
/*
var array = [],
    col = collect(fromTo(0,2), array);
    log(col());
    log(col());
    log(col());
    log("aaray" + array);
*/

//Write a filter function taht takes a generator an a predicate and produces a generator that produces only the values approved by the predicate;
/*
    var fil = filter(fromTo(0,5), function third(value){
        return (value % 3) === 0;
    });
    fil();//0
    fil()//3
    fil()//undefined
*/

function filter(fn1, fn2) {
    return function () {
        var value;
        do {
            value = fn1();
        } while (
            value !== undefined && !fn2(value)
        )
        return value;
    }
}

//ES6
function filter2(gen, predicate) {
    return function recur() {
        var value = gen();
        if (value === undefined || predicate(value)) return value;

        return recur();
    }
}
/*var fil = filter2(fromTo(0,5), function third(value){
       return (value % 3) === 0;
   });
   log(fil());//0
   log(fil());//3
   log(fil());//undefined */

//??? Write concat function 
/*
    var con = concat(fromTo(0,3), fromTo(0,2));
    con()//0
    con()//1
     con()//2
     con()//0
     con()//1
     con()//undefined
*/
function concat(fn1, fn2) {
    return function () {
        var x1 = fn1();
        if (x1 !== undefined) {
            return x1;
        } else {
            return fn2();

        }
    }
}

function concat2(gen1, gen2) {
    var gen = gen1;
    return function () {
        var value = gen();
        if (value !== undefined) {
            return value;
        }
        gen = gen2;
        return gen();
    }
}


function concat3(...gens) {
    var next = element2(gens),
        gen = next();

    return function recur() {
        var value = gen();
        if (value === undefined) {
            gen = next();
            if (gen !== undefined) {
                return recur();
            }
        }
        return value;
    }
}

/*var con = concat3(fromTo(0,3), fromTo(0,2));
                   log(con());
                   log(con());
                   log(con());
                   log(con());
                   log(con());
                   log(con());
*/

//????? Make function gensymf that makes serial numbers
/*
    var geng = gensymf("G"),
        genh = gensymf("H1");
       geng()//G1
       genh()//H1
       geng()//G2 
*/
function gensymf(alph) {
    var index = from(1);
    return function () {

        return alph + index();
    }
}

function gensymf2(prefix) {
    var number = 0;
    return function () {
        number += 1;
        return prefix + number;
    }

}
/*var geng = gensymf2("G"),
       genh = gensymf2("H");
      log(geng())//G1
      log(genh())//H1
      log(geng())//G2 
      log(geng())//G2 
      log(geng())//G2 
      log(geng())//G2 
      log(genh())//H1
*/


//??? Fibonaccif function
/*
    var fib = fibonaccif(0,1);
    fib();//0
    fib();//1
    fib();//1
    fib();//2
    fib();//3
    fib();//5

*/

function fibonacci(x, y) {
    var arr = [x, y];
    var index = 0;
    return function () {
        var current = index;
        index += 1;
        if (arr[current] !== undefined) {
            return arr[current];
        }
        else {
            var el1 = current - 1;
            var el2 = current - 2;
            arr.push(add(arr[el1], arr[el2]));
            return arr[current];
            /*
                next = x+ y;
                x = y;
                y = next;
                return next;
            */

        }
        /*Best approach
            return function(){
                var next = x;
                x=y;
                y += next;
                return next;
            }        
        */

    }
}
function fibionacci2(x, y) {
    return function () {
        var next = x;
        x = y;
        y += next;
        return next;
    }

}
/*var fib = fibionacci2(0,1);
    log(fib());//0
    log(fib());//0
    log(fib());//0
    log(fib());//0
    log(fib());//0
    log(fib());//0
*/


/*
    Write a counter function that returns an object containing two functions that implement an up/down counter, hiding the counter
    var object = counter(10),
        up = obect.up,
        down = object.down;
*/

function counter(x) {
    return {
        up: function () {
            x += 1
            return x;
        },
        down: function () {
            x -= 1
            return x;
        }
    }
}
/*var object = counter(10),
       up = object.up,
       down = object.down;

       log(up());
       log(down());
       log(down());
*/

/*
    var rev = revocable(add),
        add_rev = rev.invoke;

    add_rev(3,4); //7
    rev.revoke();
    add_rev(5,7);//undifiend    
 */

function revocable(fn) {

    return {
        invoke: function (x, y) {
            if (fn === undefined) {
                return undefined;
            }
            return fn(x, y);
        },
        revoke: function () {
            fn = undefined;
        }
    }
}
/*var rev = revocable(add),
        add_rev = rev.invoke;

    log(add_rev(3,4)); //7
    rev.revoke();
    log(add_rev(5,7));//undifiend */

function m(value, source) {
    return {
        value: value,
        source: (typeof source === 'string') ? source : String(value)
    }
}
/*
    Write a functrion addm that takes two m objects and returns an m object
    Json.stringify(addm(m(3),m(4))) // {"value":7, "source":"(3+4)"}
    JSON.strigify(addm(m(1),m(Math.PI, "pi"))) // {"value":4.14159, "source":"(1+PI)"}

*/
function addm(fn1, fn2) {
    var f = fn1;
    var g = fn2;
    return {
        value: f.value + g.value,
        source: "(" + f.source + "+" + g.source + ")"
    }
}
function addm2(fn1, fn2) {
    return m(
        fn1.value + fn2.value,
        "(" + fn1.source + "+" + fn2.source + ")"
    )
}
/*
log(JSON.stringify(addm2(m(3),m(4)))) // {"value":7, "source":"(3+4)"}
    log(JSON.stringify(addm2(m(1),m(Math.PI, "pi")))) // {"value":4.14159, "source":"(1+PI)"}
*/

/*
    Write a function liftm that takes a binary function an a string and returns a function that actas on m objects
    var addm = liftm(add, "+");
    JSON.stringify(addm(m(3), m(4))) //value 7 source (3+4)
    JSON.stringify(liftm(mul, "*")(m(3),m(4))) //value 12 source(3*4)
*/
function liftm(fn, a) {
    return function (x, y) {
        return m(
            fn(x.value, y.value),
            "(" + x.source + a + y.source + ")"
        );
    };
}
/*
var addml = liftm(add, "+");
    log(JSON.stringify(addml(m(3), m(4))) );//value 7 source (3+4)
    log(JSON.stringify(liftm(mul, "*")(m(3),m(4)))); //value 12 source(3*4)*/


/* Modify liftm todo
    var addm = liftm(add, "+");
    JSON.stringify(addm(3,4)) // value 7 source (3+4)
*/
function liftm(fn, a) {
    return function (x, y) {
        if (typeof x === "number") {
            x = m(x)
        }
        if (typeof y === "number") {
            y = m(y);
        }

        return m(
            fn(x.value, y.value),
            "(" + x.source + a + y.source + ")"
        );
    };
}
/*var addml2 = liftm(add, "+");
        log(JSON.stringify(addml2(3,4))); // value 7 source (3+4)*/


/*
    Write a function exp that evaluates simple array exressions.
    var sae = [mul, 5, 11];
    exp(sea);//55
    exp(42);//42
*/

function exp(x) {
    if (Array.isArray(x)) {
        return x[0](x[1], x[2]);
    }
    return x;
    /*
        return (Array.isArray(x) 
        ? x[0](x[1],x[2])
        : x)
    */
}
/*var sae = [mul, 5, 11];
    log(exp(sae));//55
    log(exp(42));//42*/


/*
    var nae = [Math.sqrt, [add, [square, 3], [square, 4]]];
    exp(nae)//5


*/
function exp2(arr) {
    return (Array.isArray(arr)
        ? arr[0](exp2(arr[1]), exp2(arr[2]))
        : arr
    )

}
/*var nae = [Math.sqrt, [add, [square, 3], [square, 4]]];
    log(exp2(nae));//5
*/

// ???? Write a function addg that adds from many invocations until it sees an empty invocation;
/*
    addg()//undefined
    addf(2)()//2
    addg(1)(2)()//3
*/
//recurcion
function addg(x) {
    function addc(y) {
        if (y === undefined) {
            return x;
        }
        x += y;
        return addc;
    }

    if (x === undefined) {
        return undefined;
    } else {
        return addc;
    }
}
/*log(addg())
log(addg(2)())
log(addg(2)(1)())*/
/*
    write a function that takes binary function and apply it to many invocations
*/

function liftg(x) {
    if (x === undefined) {
        return undefined;
    }
    return function (y) {
        if (y === undefined) {
            return y;
        }
        return function mulc(z) {
            if (z === undefined) {
                return y;
            }
            y = x(y, z);
            return mulc;
        }
    }
}
/*log(liftg());
log(liftg(mul)(1)());
log(liftg(mul)(1)(2)(5)());*/
/*
    Write function that returns an array
    arrayg()//[]
    arrayg(3)()[3]
    arrayg(3)(4)(5)()[3,4,5]

*/

function arrayg(x) {
    var array = [];
    if (x === undefined) {
        return array;
    }
    array.push(x);

    return function arr(y) {
        if (y === undefined) {
            return array;
        }
        array.push(y);
        return arr;
    }


}
function arrayg2(x) {
    var array = [];
    function adda(y) {
        if (y === undefined) {
            return array;
        }
        array.push(y);
        return adda;
    }
    return adda(x);

}
/*  log(arrayg2());//[]
  log(arrayg2(3)());//[3]
  log(arrayg2(3)(4)(5)(9)()) //[3,4,5]
*/

//Make a function continuize that takes a unary function, and returns a function that takes a callback and an argument
/*
    sqrtc = continuize(Math.sqrt)
    sqrtc(alert,81); //9
*/
//continious passing style,  functions are passed as parameters inside function gets arguments
function continuize(fn) {

    return function (x, y) {
        return x(fn(y));
    };
}

function continuize2(any) {
    return function (call, ...x) {
        return call(any(...x))
    }
}

/*sqrtc = continuize2(Math.sqrt)
    sqrtc(log,81); //9*/

function constructor(init) {
    var that = other_constructor(init),
        member,
        method = function () {
            //init, member. method
        };
    that.method = method;

    return that;
}
function cons2(spec) {
    let { member } = spec;
    const { other } = other_constructor(spec);
    const method = function () {
        //spec, member, other, method
    };
    return Object.freeze({
        method,
        other
    });
}
/*
make array wrapper objectwith methods get store and sppend such that an attacker cannot gget access to the private array
*/
function vector() {
    var array = [1, 2];
    return {
        get: function get(i) {
            return array[i];
        },
        store: function store(i, v) {
            array[i] = v;
        },
        append: function append(v) {
            array.push(v);
        }
    }
}

myvector = vector();

var stash;

myvector.store('push', function () {
    stash = this;
});
myvector.append();
//log("array " + stash + " s");
//identyfing vulnerabilities


/*
    Make a function that makes a publish/subscribe object. It will reliably deliver all publications to all subscribers in the right order;
*/

function pubsub() {
    var subscribers = [];
    return {
        subscribe: function (subscriber) {
            subscribers.push(subscriber);
        },
        publish: function (publications) {
            var i, length = subscribers.length;
            for (var i = 0; i < length; i += 1) {
                //fix try{
                subscribers[i](publications);
                //}
                //catch (ignore){}
            }
        }
    }
}

// problem my_pubsub.subscribe();
//solve typeof, try catch

//py_pubsub.publish = undefined;
//solution Object.freeze
// return Object.freeze{}


//my_pubsub.subscribe(function(){
//    this.length = 0;
//})

//solution subscribers.forEach(function(s){
//    try{
//        s(publication);
//   } catch (ignore);
//})

// custom message can be called before over messages
//my_pubsub.subscribe(limit(function(){
//    my_pubsub.publish("Out of order");
//}))

//Solution
//solution subscribers.forEach(function(s){
//   setTimeout(function(){
//    s(publication);
//  },0)
//        
//})
function factorial(n) {
    if (n < 2) {
        return n;
    }

    return n * factorial(n - 1);
    //return factorial(n - 1);

}

//log(factorial(4));

const bubbleSort2 = (nums) => {

    do {
        var swapped = false;
        for (let i = 0, l = nums.length; i < l; i += 1) {

            if (nums[i] > nums[i + 1]) {
                const temp = nums[i];
                nums[i] = nums[i + 1];
                nums[i + 1] = temp;
                log(nums);
                swapped = true;
            }
        }
    }
    while (swapped)
}
function bubbleSort(arr) {

    do {
        var swapped = false;
        for (var i = 0; i < arr.length; i += 1) {
            var x = arr[i];
            var y = arr[i + 1];
            if (x > y && y !== undefined) {
                arr[i + 1] = x;
                arr[i] = y;
                log("SWAPPED!");
                log("array " + arr + " || ");
                swapped = true;

            }
        }

    }
    while (swapped == true);
    return arr;
}
//log(bubbleSort([10,5,3,8,2,6,4,7,9,1]))


//
function insertionSort(nums) {
    for (var i = 0; i < nums.length; i += 1) {

        for (var j = i + 1; j < nums.length; j += 1) {
            if (nums[j] < nums[i]) {
                var temp = nums[i];
                nums[i] = nums[j];
                nums[j] = temp;
            }
        }
    }

}
//difference is insertion2 takes first argument as sorted(j0) and then takes unsorted array elements(i1) 
insertionSort([10, 5, 3, 8, 2, 6, 4, 7, 9, 1])

function insertionSort2(nums) {
    var counter = 0
    for (var i = 1; i < nums.length; i++) {
        for (var j = 0; j < i; j++) {
            if (nums[i] < nums[j]) {
                var spliced = nums.splice(i, 1);
                nums.splice(j, 0, spliced[0]);
            }
        }
    }
    log("counter: " + counter);
}
insertionSort2([10, 5, 3, 8, 2, 6, 4, 7, 9, 1])
