/* 
Not usefull in production but helps to show how people think of sorting

Outer loop is for tracking if anything was swapped

Big O = n2;
*/


const bubbleSort = (nums) => {
    let comparisonTimes = 0;
    do {
        var swapped = false;
        nums.forEach((item, index) => {
            console.log("arr: ", nums);
            comparisonTimes++;
            if(item > nums[index + 1]){
                [nums[index],nums[index + 1]] = [nums[index + 1], nums[index]];
                swapped = true;
            }
        });
    } while(swapped);
    console.log(`Comaprison times: ${comparisonTimes}`);
}
bubbleSort([10,5,3,8,2,6,4,7,9,1]);
