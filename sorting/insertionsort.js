/*
    Insertion sort is good when arrays are almost sorted
    But it doesn't perform well when arrays are not sorted at all

    Big O = n2;
*/
const insertionSortOneArray = (nums) => {
    let comparisons = 0;
    //Starting from second item in array. First is sorted.
    for(let i = 1, length = nums.length; i < length; i++){
        //Comparing to i because nums array before i index is considered as sorted array
        for(let j = 0; j < i; j++){
            comparisons++;
            console.log("Array: ", nums);
            if(nums[i] < nums[j]){
                let splicedValue = nums.splice(i,1)[0];
                nums.splice(j,0, splicedValue);
            }
        }
    }
    console.log("Sorted using single array: ", nums);
    console.log("Comaprisons: ", comparisons);
}

const insertionSortTwoArrays = (arr) => {
    let comparisons = 0;
    var [first, ...nums] = arr;
    var sortedArray = [first];
    for(let i = 0, length = nums.length; i < length; i++){
        for(let j = 0, length = sortedArray.length; j < length; j++){
            comparisons++;
            console.log("Array: ", nums);
            if(nums[i] < sortedArray[j]){
                sortedArray.splice(j,0,nums[i]);
                break;
            }
        }
    }
    console.log("Sorted using two arrays: ", sortedArray);
    console.log("Comaprisons: ", comparisons);
}
insertionSortTwoArrays([10,5,3,8,2,6,4,7,9,1]);
insertionSortOneArray([10,5,3,8,2,6,4,7,9,1]);